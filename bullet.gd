
extends KinematicBody2D

var sword
var acc = 0.001
var velocity = Vector2(0, 0)
var pos
var direction
var damage = 1 setget _set_damage
var health = 1 setget _set_health
var size = 1
var blood_text
var timer
const invincibility_period = 0.8
var coin_scene = load("res://coin.scn")

func _set_health(h):
	health = h
	size = h
	scale(Vector2(health*0.7, health*0.7))
func _set_damage(d):
	damage = d 

func _ready():
	set_fixed_process(true)
	sword = get_parent().get_node("sword")
	timer = Timer.new()
	add_child(timer)
	timer.set_one_shot(true)
	timer.set_wait_time(invincibility_period)

func _handle_collisions():
	var collider = get_collider()
	if (collider._get_name() == "sword" and collider.health > 0):
		collider._collided(get_collision_pos(), self)
	elif (collider._get_name() == "sw"):
		collider.get_parent()._enemy_on_sword(self, get_collision_pos(), get_collision_normal())

func _drop_money():
	var pos = get_pos()
	for i in range(0, size):
		var node = coin_scene.instance()
		pos.x += 5
		node.set_pos(pos)
		get_parent().add_child(node)

func _die():
	get_parent()._add_point(size)
	#_drop_money()
	queue_free()

func _get_damage():
	return damage

func _spill_blood(col_normal, damage, to_leave_marks, to_attach):
	var particles = get_parent().get_node("blood_particles").duplicate()
	particles._set_lifetime(0.5)
	var currV = Vector2(0, 1)
	var rot = currV.angle_to(col_normal)
	particles.rotate(rot)
	particles.set_amount(32*damage*2)
	particles.show()
	if (to_attach):
		particles.set_pos(Vector2(0, 0))
		add_child(particles)
	else:
		particles.set_pos(get_pos())
		get_parent().add_child(particles)
	if (to_leave_marks):
		var r = 10*size
		for i in range(0, r):
			var pixel = get_parent().get_node("blood").duplicate()
			get_parent().add_child(pixel)
			var pix_pos = get_pos()+col_normal.rotated(rand_range(-PI/2, PI/2))*damage*4
			pixel.set_pos(pix_pos)
			pixel.show()

func _damage(amount, col_normal):
	if (timer.get_time_left() > 0):
		return
	health -= amount
	if (health <= 0):
		_die()
		_spill_blood(-col_normal, amount, true, false)
	else:
		velocity = -col_normal*0.5*damage
		_spill_blood(-col_normal, amount, false, true)
	timer.start()

func _fixed_process(delta):
	pos = get_pos()
	var sword_pos = sword.get_pos()
	var dir = sword_pos - pos
	dir.normalized()
	
	velocity += dir*acc*delta
	move(velocity)
	
	if (is_colliding()):
		_handle_collisions()

func _get_name():
	return "bullet"

