
extends Node2D

var timer
var timer_interval = 1
var screen_size
var ball_scene = load("res://ball_scene.scn")
var points = 0
var best = 0
var prev_best = 0
const in_diff_per_second = 0.5
const in_diff_interval = 5
var difficulty_per_second = in_diff_per_second
var difficulty = 1
var real_time_diff = 1
var diff_interval = in_diff_interval
var diff_passed = 0
var money = 0
var health_text
var t_after_pause = 0
var gj_api
var login_mode = true #login as gamejolt user (default)
var guest_name = ""
var game_token = ""
var username = ""
var game_id = "75807"
var private_key = "d6cebd333695ed0ea21821b759ac63a5"

func _init():
	gj_api = GamejoltAPI.new()
	gj_api.init(game_id, private_key)
	load_credentials()
	randomize()

func _ready():
	timer = get_node("spawn_timer")
	timer.set_wait_time(timer_interval)
	
	screen_size = get_viewport_rect().size
	get_node("blood_particles")._set_lifetime(-1)
	best = prev_best
	update_best()
	get_node("energy").set_val(100)
	set_process(true)
	
	if (username != "" and game_token != ""):
		get_node("menu/username").set_text(username)
		var g_token = get_node("menu/game_token")
		g_token.set_secret(true)
		g_token.set_text(game_token)
	
	get_tree().set_pause(true)

func update_best():
	get_node("best").set_text("Best: " + str(best))

func _process(delta):
	if (Input.is_action_pressed("quit")):
		OS.get_main_loop().quit()
	t_after_pause += delta
	if (Input.is_action_pressed("pause")):
		if (get_tree().is_paused() and t_after_pause > 2):
			get_tree().set_pause(false)
			t_after_pause = 0
		elif (t_after_pause > 2):
			get_tree().set_pause(true)
			t_after_pause = 0

func _on_Timer_timeout():
	diff_passed += 1
	if (diff_passed > diff_interval):
		difficulty += 0.1
		diff_passed = 0
	real_time_diff += difficulty_per_second
	difficulty_per_second += 0.02
	if (real_time_diff >= difficulty):
		var en_health = randi() % int(round(difficulty))+1
		if (en_health == 0):
			en_health = 1
		real_time_diff -= en_health 
		var enemy = ball_scene.instance()
	
		var border = rand_range(0,4)
		var pos = Vector2(0, 0)
		if (border >= 0 and border < 1):
			pos = Vector2(rand_range(0, screen_size.x), -1)
		elif (border >= 1 and border < 2):
			pos = Vector2(rand_range(0, screen_size.x), screen_size.y+5)
		elif (border >= 2 and border < 3):
			pos = Vector2(0, rand_range(0, screen_size.y))
		elif (border >= 3 and border < 4):
			pos = Vector2(screen_size.x+5, rand_range(0, screen_size.y))
		enemy.set_pos(pos)
		enemy._set_health(en_health)
		add_child(enemy)

func _add_point(point):
	if (get_node("sword").health <= 0):
		return
	points += point
	var label = get_node("points")
	label.set_text(str(points))
	
func _add_money(amount):
	money += amount
	var label = get_node("money")
	label.set_text(str(money))

func _game_over():
	get_node("game_over").show()
	get_node("sword")._set_stop(true)
	if (points > best):
		best = points
		update_best()
	if (login_mode and points > 0):
		call_deferred("send_score", points)
	elif(guest_name != "" and points > 0):
		call_deferred("send_score", points, "", guest_name)
	

func _on_Button_restart():  #pressed
	var label = get_node("points")
	label.set_text("0")
	var ball = preload("res://bullet.gd")
	var bloodText = preload("res://media/blood_pixel.png")
	for child in get_children():
		if (child extends ball or (child extends Sprite and child.get_texture() == bloodText and child.is_visible())):
			child.queue_free()
	get_node("game_over").hide()
	difficulty_per_second = in_diff_per_second
	difficulty = 1
	real_time_diff = 1
	diff_interval = in_diff_interval
	diff_passed = 0
	money = 0
	points = 0
	get_node("sword")._restart()
	

func _on_play_button_pressed():
	get_node("menu").hide()
	get_node("tittle").hide()
	get_tree().set_pause(false)


func _on_login_button_pressed():
	if (login_mode):
		username = get_node("menu/username").get_text()
		game_token = get_node("menu/game_token").get_text()
		if (!gj_api.login(username, game_token)):
			var message = gj_api.get_status()
			get_node("menu/messages").set_text(message)
			get_node("menu/messages").update()
			return
		else:
			save_credentials()
			best = get_prev_user_best()
			update_best()
	else:
		guest_name = get_node("menu/username").get_text()
		best = get_prev_guest_best(guest_name)
		update_best()
	_on_play_button_pressed()

func _on_guest_button_pressed():
	if (login_mode):
		login_mode = false
		get_node("menu/login_button").set_text("Play as guest")
		get_node("menu/guest_button").set_text("Gamejolt login")
		get_node("menu/username").set_text("Guest_name")
		get_node("menu/game_token").hide()
	else:
		login_mode = true
		get_node("menu/login_button").set_text("Login")
		get_node("menu/guest_button").set_text("Play as guest")
		get_node("menu/username").set_text("Username")
		get_node("menu/game_token").show()


func _on_game_token_text_entered( text ):
	_on_login_button_pressed()

func _on_username_text_entered( text ):
	_on_login_button_pressed()

func _on_game_token_focus_enter():
	get_node("menu/game_token").set_text("")
	get_node("menu/game_token").set_secret(true)

func _on_username_focus_enter():
	get_node("menu/username").set_text("")

func send_score(points, scoreboard = "", guest_name = ""):
#	message("Sending score..", 0.1, 0)
	var success = gj_api.send_score(points, scoreboard, guest_name)
	if (!success):
		var status = gj_api.get_status()
#		message(status+" Trying again..", 1, false)
		if (status == "GamejoltAPI::SendScore(): not connected"):
			gj_api.init(game_id, private_key)
		success = gj_api.send_score(points, scoreboard, guest_name)
		if (!success):
			status = gj_api.get_status()
			if (status == "GamejoltAPI::SendScore(): not connected"):
				gj_api.init(game_id, private_key)
			success = gj_api.send_score(points, scoreboard, guest_name)
			if (!success):
				status = gj_api.get_status()
				if (status == "GamejoltAPI::SendScore(): not connected"):
					gj_api.init(game_id, private_key)
				success = gj_api.send_score(points, scoreboard, guest_name)
#	if (!success):
#		message("Failed to send score: " + gj.get_status(), 10 ,false)
#	else:
#		message("Score sent", 10, false)

func save_credentials():
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://credentials.bin",File.WRITE,OS.get_unique_ID())
	f.store_var(username)
	f.store_var(game_token)
	f.close()

func load_credentials():
	var f = File.new()
	var err = f.open_encrypted_with_pass("user://credentials.bin", File.READ, OS.get_unique_ID())
	if (!f.is_open()):
		return
	username = f.get_var()
	game_token = f.get_var()
	f.close()

func _on_highscores_pressed():
	get_node("menu").hide()
	get_node("scoreboard").show()
	if (gj_api.get_status() != GamejoltAPI.STATUS_OK):
		return
	var scores = gj_api.get_scores("", 100)
	if (gj_api.get_status() != GamejoltAPI.STATUS_OK):
		return
	var ind = 0
	var place = get_node("scoreboard/ScrollContainer/VBoxContainer/place")
	while (scores.has(ind)):
		place = place.duplicate()
		get_node("scoreboard/ScrollContainer/VBoxContainer").add_child(place)
		place.get_node("no").set_text(str(ind+1))
		var name
		if (scores[ind]["user"] == ""):
			name = scores[ind]["guest"]
		else:
			name = scores[ind]["user"]
		place.get_node("username").set_text(name)
		place.get_node("score").set_text(scores[ind]["score"])
		ind += 1

func get_prev_user_best():
	if (username == ""):
		return -1
	if (gj_api.get_status() != GamejoltAPI.STATUS_OK):
		return -1
	return gj_api.get_user_best()

func get_prev_guest_best(guest_name):
	if (guest_name == ""):
		return -1
	return gj_api.get_guest_best(guest_name)

func _on_back_pressed():
	get_node("scoreboard").hide()
	get_node("menu").show()
