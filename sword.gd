
extends KinematicBody2D

var screen_size
var speed = 200
var rot_speed = 6*PI  #in radians per second
var rotation = 0
var velocity = Vector2(0, 0)
var sword_damage = 1
const initial_health = 5
var health = initial_health
var timer
const invincibility_period = 2
var color
var health_text
var max_energy = 4*PI
var energy = max_energy
var energy_rest_speed = PI*1.2 #per second
var prev_rotation = "none"
var prev_rot_ang
var sw

class MouseTouchInput:
	var input = "none" setget set_input, get_input #rotate_left, rotate_right, flip_up or flip_down
	var prev_pos = Vector2(0, 0)
	var curr_pos = Vector2(0, 0)
	var start_pos
	var pressed = false
	func _process(delta):
		if (Input.is_action_pressed("start_rot") and !pressed):
			pressed = true
			start_pos = curr_pos
		elif (!Input.is_action_pressed("start_rot") and pressed):
			pressed = false
	func _input(event):
		if (event.type == InputEvent.MOUSE_BUTTON):
			curr_pos = event.pos
		elif (event.type == InputEvent.MOUSE_MOTION and pressed):
			curr_pos = event.pos
			var diff = event.pos - start_pos
			if (abs(diff.x) > abs(diff.y) and abs(diff.x) > 10):
				if (diff.x > 1):
					input = "rotate_right"
				else:
					input = "rotate_left"
			elif (abs(diff.y) > 1):
				if (diff.y > 1):
					input = "flip_up"
				else:
					input = "flip_down"
			prev_pos = event.pos
	func set_input(inp):
		input = inp
	func get_input():
		var inp = input
		input = "none"
		return inp
var mt_input

func _ready():
	screen_size = get_parent().get_viewport_rect().size
	set_fixed_process(true)
	set_process(true)
	set_process_input(true)
	timer = get_node("health_timer")
	timer.set_one_shot(true)
	color = get_node("head").get_modulate()
	health_text = get_parent().get_node("health_bar/TextureFrame").duplicate()
	_update_energy()
	mt_input = MouseTouchInput.new()
	sw = get_node("sw")
	
func _update_energy():
	get_parent().get_node("energy").set_val(energy/max_energy*100)

func _input(ev):
	mt_input._input(ev)

func _damage(amount):
	if (timer.get_time_left() > 0 or health <= 0):
		return
	health -= amount
	var health_bar = get_parent().get_node("health_bar")
	if (health_bar.get_child_count() > 0):
		var children = health_bar.get_children()
		for child in children:
			if (child.get_type() == "TextureFrame"):
				child.queue_free()
				break
	if (health <= 0):
		get_parent()._game_over()
	timer.set_wait_time(invincibility_period)
	timer.start()

func _is_in_head(pos, scale_size = 1):
	var head_node = get_node("head")
	var size = head_node.get_texture().get_size()*scale_size
	var head_rect = Rect2(head_node.get_global_pos() - size/2, size)
	if (head_rect.has_point(pos)):
		return true
	else:
		return false

func _handle_collisions():
	if (!is_colliding()):
		return
	var obj = get_collider()
#	var pos = get_collision_pos()
	if (obj._get_name() == "bullet"):
		_damage(obj._get_damage())

func _enemy_on_sword(obj, pos, col_normal = Vector2()):
	if (col_normal == Vector2()):
		col_normal = sw.get_collision_normal()
	if (rotation == 0 and velocity.length() > 0):
		obj._damage(sword_damage, col_normal)
	elif (rotation != 0):
		obj._damage(sword_damage+1, col_normal)

func _handle_sw_collisions():
	if (!sw.is_colliding()):
		return
	var obj = get_collider()
	_enemy_on_sword(obj, get_collision_pos())

func _get_rot_input():
	var input = mt_input.get_input()
	if (Input.is_action_pressed("flip_down") or input == "flip_down"):
		return "flip_down"
	elif (Input.is_action_pressed("flip_up") or input == "flip_up"):
		return "flip_up"
	elif (Input.is_action_pressed("rotate_left") or input == "rotate_left"):
		return "rotate_left"
	elif (Input.is_action_pressed("rotate_right") or input == "rotate_right"):
		return "rotate_right"
	else:
		return "none"
		
func _fix_rot(rotation):
	#if (rotation > PI):
	#	rotation = 0
	var z = abs(0 - abs(rotation))
	var pi2 = PI/2 - rotation
	var pim2 = abs(-PI/2 -rotation)
	var pi = PI - rotation
	var mpi = abs(-PI-rotation)
	var d270 = abs(PI*3/2 - rotation)
	var md270 = abs(-PI*3/2 - rotation)
	var smallest = min(pi2, min(pim2, min(pi, min(mpi, min(d270, min(z, md270))))))
	if (smallest == pi2):
		rotation = PI/2
	elif (smallest == pim2):
		rotation = -PI/2
	elif (smallest == pi):
		rotation = PI
	elif (smallest == mpi):
		rotation = -PI
	elif (smallest == z):
		rotation = 0
	elif (smallest == d270):
		rotation = PI*3/2
	elif (smallest == md270):
		rotation = -PI*3/2
	return rotation

func _process(delta):
	if (energy < max_energy):
		energy += energy_rest_speed*delta
	_update_energy()
	if (energy <= PI/2):
		return
	var input = _get_rot_input()
	var currR = sw.get_rot()
	if (rotation == 0 and input != "none"):
		var currV = Vector2(1, 0).rotated(currR)
		if (prev_rotation != input):
			if (input == "flip_down"):
				var v = Vector2(0, -1)
				rotation = currV.angle_to(v)
			elif (input == "flip_up"):
				var v = Vector2(0, 1)
				rotation = currV.angle_to(v)
			elif (input == "rotate_left"):
				var v = Vector2(-1, 0)
				rotation = currV.angle_to(v)
			elif (input == "rotate_right"):
				var v = Vector2(1, 0)
				rotation = currV.angle_to(v)
			#rotation = _fix_rot(rotation)
			prev_rotation = input
			prev_rot_ang = rotation
		elif (prev_rotation == input and input != "none"):
			rotation = prev_rot_ang
#	else:
#		currR = _fix_rot(currR)
#		sw.set_rot(currR)


func _fixed_process(delta):
	var dir = Vector2(0, 0)
	if (Input.is_action_pressed("move_down")): #and check for touch input
		dir.y = 0.5
	if (Input.is_action_pressed("move_left")):
		dir.x = -0.5
	if (Input.is_action_pressed("move_right")):
		dir.x = 0.5
	if (Input.is_action_pressed("move_up")):
		dir.y = -0.5
	
	if (rotation != 0):
		var r
		if (rotation>0):
			r = rot_speed*delta
		else:
			r = -rot_speed*delta
		if (abs(rotation) < abs(r)):
			r = rotation
		sw.rotate(r)
		rotation -= r
		energy -= abs(r)

	dir = dir.normalized()
	velocity = dir*speed*delta
	move(velocity)
	
	if (is_colliding()):
		_handle_collisions()
	if (sw.is_colliding()):
		_handle_sw_collisions()

func _set_stop(to_stop):
	var sprite = get_node("head")
	sprite.set_modulate(Color(1, 0, 0))
	var r = _fix_rot(sw.get_rot())
	sw.set_rot(r)
	set_fixed_process(false)
	set_process(false)

func _get_name():
	return "sword"

func _collided(pos, obj):
	_damage(obj.damage)

func _restart():
	var sprite = get_node("head")
	sprite.set_modulate(color)
	health = initial_health
	prev_rotation = "none"
	var ang = Vector2(0, 1).rotated(get_rot()).angle_to(Vector2(0, 1))
	rotate(ang)
	rotation = 0
	energy = max_energy
	_update_energy()
	
	var health_bar = get_parent().get_node("health_bar")
	for i in range(0, health):
		health_bar.add_child(health_text.duplicate())
	timer.start()
	set_fixed_process(true)
	set_process(true)
