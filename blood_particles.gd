
extends Particles2D

var timer = Timer.new()
var lifetime = 2 setget _set_lifetime

func _set_lifetime(value):
	lifetime = value
	if (value == -1):
		timer.stop()
		return
	else:
		timer.set_wait_time(lifetime)
		timer.start()

func _ready():
	add_child(timer)
	timer.set_one_shot(true)
	timer.set_wait_time(lifetime)
	timer.connect("timeout", self, "_timeout")
	timer.start()

func _timeout():
	queue_free()


